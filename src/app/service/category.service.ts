import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Category } from "../entity/category";
import { HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private url = 'http://localhost:3000/category/';

  constructor(private http:HttpClient) { }

  findAll():Observable<Category[]> {
    return this.http.get<Category[]>(this.url);
  }

  find(id:number): Observable<Category> {
    return this.http.get<Category>(this.url + id);
  }

  add(category:Category): Observable<Category> {
    return this.http.post<Category>(this.url, category);
  }
}
