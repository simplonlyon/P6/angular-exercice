import { Component } from '@angular/core';
import { Link } from './menu/link';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  mainMenu:Link[] = [
    {
      label:'About',url:'/about'
    },
    {
      label:'List',url:'/list'
    }
  ];
  /*
  secondMenu:Link[] = [
    {
      label:'Bloup',url:'/blip'
    },
    {
      label:'Test',url:'/test'
    },
    {
      label:'Doud',url:'/test'
    },
    {
      label:'idou',url:'/test'
    }
  ];
  */
}
