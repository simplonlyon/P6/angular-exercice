import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Product } from '../entity/product';
import { Category } from '../entity/category';

@Component({
  selector: 'app-form-product',
  templateUrl: './form-product.component.html',
  styleUrls: ['./form-product.component.css']
})
export class FormProductComponent implements OnInit {
  @Input()
  product:Product = {label: null, category: null, price: null};
  @Input()
  categories:Category[];
  @Output()
  formSubmit = new EventEmitter<Product>();

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    this.formSubmit.emit(this.product);
  }

}
